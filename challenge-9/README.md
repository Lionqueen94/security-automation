# Challenge 9

This challenge describes how to run unprivileged container!

----

### Change your directory to the flask-app dir

##### command:

```bash
cd flask-app
```
--

### Build the lab!

##### command:

```bash 
docker build -t cmd .
```

#### output:

```bash
Sending build context to Docker daemon  1.416MB
Step 1/7 : FROM alpine:3.7
 ---> 6d1ef012b567
Step 2/7 : MAINTAINER Glenn ten Cate <glenn.ten.cate@owasp.org>
 ---> Using cache
 ---> 7a52d33b0c9d
Step 3/7 : RUN apk update --no-cache && apk add python3 python3-dev py3-pip git bash imagemagick
 ---> Running in d8fd4c9265b4
fetch http://dl-cdn.alpinelinux.org/alpine/v3.7/main/x86_64/APKINDEX.tar.gz
fetch http://dl-cdn.alpinelinux.org/alpine/v3.7/community/x86_64/APKINDEX.tar.gz
v3.7.3-180-g5372bc29f3 [http://dl-cdn.alpinelinux.org/alpine/v3.7/main]
v3.7.3-183-gcc9ad2b48d [http://dl-cdn.alpinelinux.org/alpine/v3.7/community]
......... snip ........
```

----

### Run and exploit the lab

##### command:

```bash
docker run -p0.0.0.0:5000:5000 cmd
```

##### output:

```bash
 * Running on http://0.0.0.0:5000/ (Press CTRL+C to quit)
 * Restarting with stat
 * Debugger is active!
 * Debugger PIN: 200-613-924
......... snip ..........
```

After running the lab use the command execution to read the
"secret-file" content

----