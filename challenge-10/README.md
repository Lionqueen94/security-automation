# Challenge 10 - Docker in docker

### Pull the Jenkins container

##### command:

```bash
docker pull jenkins/jenkins:lts
```

#### output:

```bash
Using default tag: latest
latest: Pulling from jenkins/jenkins:lts
cbdbe7a5bc2a: Downloading [=================>                                 ]    961kB/2.813MB
26ebcd19a4e3: Download complete 
8341bd19193b: Downloading [==>                                                ]  846.3kB/20.69MB
ecc595bd65e1: Waiting 
4b1c9d8f69d2: Waiting 
4acb96206c62: Waiting 
88da57106cb7: Waiting 
......... snip ........
```
----

### Run the Jenkins container

##### command:

```bash
docker run -p 8080:8080 -p 5000:5000  --user root \
-v jenkins_home-0:/var/jenkins_home \
-v /var/run/docker.sock:/var/run/docker.sock \
-v $(which docker):$(which docker) \
jenkins/jenkins:lts
```

#### IMPORTANT:

Jenkins wil generate a password for first login!

```bash
*************************************************************
*************************************************************
*************************************************************

Jenkins initial setup is required. An admin user has been created and a password generated.
Please use the following password to proceed to installation:

991debb865ad4c80a3ae24f7dc0d666e

This may also be found at: /var/jenkins_home/secrets/initialAdminPassword

*************************************************************
*************************************************************
*************************************************************
......... snip ........
```

### Objective:

Get Code execution on the Jenkins docker instance
trough docker sock

----