## Spoiler

```bash
stages:
- build
build:
  stage: build
  image: docker:stable
  services:
    - docker:dind
  script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - cd challenge-13/zap
    - docker build -t registry.gitlab.com/riiecco1/security-automation/zapscan-auto-build .
    - docker push registry.gitlab.com/riiecco1/security-automation/zapscan-auto-build
```