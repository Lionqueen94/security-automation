# Challenge 1

This challenge describes how to run your first hello-world container

Maintained by: the Docker Community

----

### Pull the image:

##### command:

```bash
docker pull hello-world
```

##### output:

```bash
Using default tag: latest
latest: Pulling from library/hello-world
0e03bdcc26d7: Pull complete 
Digest: sha256:4cf9c47f86df71d48364001ede3a4fcd85ae80ce02ebad74156906caff5378bc
Status: Downloaded newer image for hello-world:latest
docker.io/library/hello-world:latest
```
----

### Run the image:

##### command:

```bash
docker run hello-world
```

##### output:

```bash
Hello from Docker!
This message shows that your installation appears to be working correctly.

To generate this message, Docker took the following steps:
 1. The Docker client contacted the Docker daemon.
 2. The Docker daemon pulled the "hello-world" image from the Docker Hub.
    (amd64)
 3. The Docker daemon created a new container from that image which runs the
    executable that produces the output you are currently reading.
 4. The Docker daemon streamed that output to the Docker client, which sent it
    to your terminal.

To try something more ambitious, you can run an Ubuntu container with:
 $ docker run -it ubuntu bash

Share images, automate workflows, and more with a free Docker ID:
 https://hub.docker.com/

For more examples and ideas, visit:
 https://docs.docker.com/get-started/
```
----

### See the image on your local device:

##### command:

``` bash
docker images
```

#### output:

```bash
REPOSITORY                       TAG                 IMAGE ID            CREATED             SIZE
hello-world                      latest              bf756fb1ae65        8 months ago        13.3kB
```

--------

### Finally remove the local image:

##### command:

```bash
docker rmi bf756fb1ae65
```

##### output:

```bash
Untagged: hello-world:latest
Untagged: hello-world@sha256:4cf9c47f86df71d48364001ede3a4fcd85ae80ce02ebad74156906caff5378bc
Deleted: sha256:bf756fb1ae65adf866bd8c456593cd24beb6a0a061dedf42b26a993176745f6b
```